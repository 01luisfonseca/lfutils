"use strict";

var waittime = 10000; // In millis

var requestCached = [];

var xhr = require('./xhr.service');

module.exports = requester;

function requester(baseUrl, config) {
  var _this = this;

  waittime = _.get(config, 'waitTime', waittime);
  var generalConfig = {
    baseUrl: baseUrl
  };
  return {
    get: function get(endpoint, headers) {
      return new Promise(function (resolve, reject) {
        var requestCache = requestCached.find(function (req) {
          return req.url === endpoint;
        });
        var dataResolved = requestCache ? requestCache.data && Date.now() - requestCache.timeStamp < waittime ? requestCache.data : null : null;

        if (!requestCache) {
          // Create new cache if not exist
          if (endpoint.charAt(0) === '/' && baseUrl.charAt(baseUrl.length - 1) === '/') endpoint = endpoint.substr(1);
          requestCache = {
            url: baseUrl + endpoint,
            data: null,
            timeStamp: Date.now(),
            inProgress: false,
            dataUpdated: function dataUpdated(cb) {
              var inter = setInterval(function () {
                if (!_this.inProgress) {
                  clearInterval(inter);
                  cb(_this.data);
                }
              }, 10);
            }
          };
          requestCached.push(requestCache);
        } else if (requestCache.inProgress) {
          // If is in progress
          return requestCache.dataUpdated(function (dt) {
            resolve(requestCache.data);
          });
        }

        if (dataResolved) return resolve(dataResolved);
        requestCache.data = null;
        requestCache.inProgress = true;
        return ajaxGenerator('get', endpoint, undefined, headers).then(function (dt) {
          requestCache.data = dt;
          requestCache.inProgress = false;
          return resolve(dt);
        })["catch"](function (err) {
          requestCache.inProgress = false;
          return reject(err);
        });
      });
    },
    put: function put(endpoint, data, headers) {
      return ajaxGenerator('put', endpoint, data, headers);
    },
    patch: function patch(endpoint, data, headers) {
      return ajaxGenerator('patch', endpoint, data, headers);
    },
    post: function post(endpoint, data, headers) {
      return ajaxGenerator('post', endpoint, data, headers);
    },
    "delete": function _delete(endpoint, headers) {
      return ajaxGenerator('delete', endpoint, undefined, headers);
    },
    options: function options(endpoint, headers) {
      return ajaxGenerator('options', endpoint, undefined, headers);
    },
    head: function head(endpoint, headers) {
      return ajaxGenerator('head', endpoint, undefined, headers);
    },
    manual: ajaxGenerator
  };
  /**
   * Request generator for ajax XMLHTTPRequest
   * @param {String} method Method of request
   * @param {String} endpoint Enpoint of server
   * @param {Object} data Array or Object to send
   * @param {Object} headers Properties of request in server
   */

  function ajaxGenerator(method, endpoint, data, headers) {
    var objectToRequest = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
    objectToRequest = Object.assign({
      responseType: 'json'
    }, objectToRequest);
    if (endpoint && endpoint.charAt(0) === '/' && baseUrl.charAt(baseUrl.length - 1) === '/') endpoint = endpoint.substr(1);
    if (!headers) headers = {};
    var config = {
      method: method,
      data: data,
      // eslint-disable-next-line
      url: baseUrl + (endpoint ? endpoint : '')
    };
    config.headers = headers;

    for (var key in generalConfig) {
      if (generalConfig.hasOwnProperty(key)) {
        var hdata = generalConfig[key];
        config[key] = hdata;
      }
    }

    return xhr(Object.assign(config, objectToRequest));
  }
}