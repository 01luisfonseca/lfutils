"use strict";

module.exports = {
  setBlobUrl: function setBlobUrl(fnc) {
    if (typeof fnc === 'function') {
      return URL.createObjectURL(new Blob(['(', fnc.toString(), ')()'], {
        type: 'application/javascript'
      }));
    }
  },
  setWorker: function setWorker(fnc) {
    var url = this.setBlobUrl(fnc);

    if (url) {
      var worker = new Worker(url);
      URL.revokeObjectURL(url);
      return worker;
    }
  }
};