"use strict";

module.exports = unitToSystem;

function unitToSystem(config) {
  var acum = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var unit = config.unit || '';

  try {
    if (typeof config.number !== 'number') return '0';
    return config.number < Math.pow(config.base, config.expStep) ? "".concat(config.number, " ").concat(config.system && config.system[acum] ? config.system[acum] : '').concat(unit) : unitToSystem(Object.assign(config, {
      number: Number((config.number / Math.pow(config.base, config.expStep)).toFixed(2))
    }), acum + config.expStep);
  } catch (error) {
    console.error(error);
    return '0';
  }
}