"use strict";

var Exif = require('@fengyuanchen/exif');

var localForage = require('localforage');

function imageRotator(file, resolveFile) {
  var _this = this;

  return new Promise(function (resolve, reject) {
    new Exif(file, {
      done: function done(tags) {
        _this.dataToURL(file).then(function (info) {
          var orientation = tags.Orientation;
          var img = document.createElement('img');
          var canvas = document.createElement('canvas');
          img.src = info.url;

          img.onload = function () {
            transformOrientation(img, canvas, orientation);
            canvas.toBlob(function (blob) {
              var staticFile = new File([blob], file.name, {
                type: 'image/jpeg'
              });
              if (resolveFile) resolve(staticFile);else _this.dataToURL(staticFile).then(resolve, reject);
            }, 'image/jpeg', 1);
          };
        });
      },
      fail: function fail(message) {
        console.error(message);
        console.log('Image not require transform');
        if (resolveFile) resolve(file);else _this.dataToURL(file).then(resolve)["catch"](reject);
      }
    });
  });
}

function transformOrientation(img, canvas, orientation) {
  // Set variables
  var ctx = canvas.getContext('2d');
  var width = img.width,
      height = img.height; // set proper canvas dimensions before transform & export

  if (orientation >= 5) {
    canvas.width = height;
    canvas.height = width;
  } else {
    canvas.width = width;
    canvas.height = height;
  } // transform context before drawing image


  switch (orientation) {
    case 2:
      ctx.transform(-1, 0, 0, 1, width, 0);
      break;

    case 3:
      ctx.transform(-1, 0, 0, -1, width, height);
      break;

    case 4:
      ctx.transform(1, 0, 0, -1, 0, height);
      break;

    case 5:
      ctx.transform(0, 1, 1, 0, 0, 0);
      break;

    case 6:
      ctx.transform(0, 1, -1, 0, height, 0);
      break;

    case 7:
      ctx.transform(0, -1, -1, 0, height, width);
      break;

    case 8:
      ctx.transform(0, -1, 1, 0, 0, width);
      break;

    default:
      ctx.transform(1, 0, 0, 1, 0, 0);
  } // Draw img into canvas


  ctx.drawImage(img, 0, 0, width, height);
}