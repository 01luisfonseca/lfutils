"use strict";

var _ = require('lodash');

var resizeFileImageWithCanvas = require('./resizeFileImageWithCanvas.service');

var base64ToFile = require('./base64ToFile.service');

module.exports = videoThumbnailer;

function videoThumbnailer(videoB64, name, params) {
  var secondOfThumbnail = _.get(params, 'secondOfThumbnail', 3);

  return new Promise(function (resolve, reject) {
    if (typeof videoB64 !== 'string') throw new Error('Video source param is not string');
    var videoTag = document.createElement('video');
    var canvas = document.createElement("canvas");
    videoTag.src = videoB64;
    videoTag.addEventListener('loadedmetadata', function () {
      canvas.width = videoTag.videoWidth;
      canvas.height = videoTag.videoHeight;
      videoTag.currentTime = secondOfThumbnail;
    });
    videoTag.addEventListener('timeupdate', function () {
      name = "".concat(name, ".png");
      var ctx = canvas.getContext('2d');
      ctx.drawImage(videoTag, 0, 0, videoTag.videoWidth, videoTag.videoHeight);
      resizeFileImageWithCanvas(base64ToFile(canvas.toDataURL(), {
        name: name
      }), Object.assign({
        height: 400,
        width: 400
      }, params)).then(resolve)["catch"](reject);
    });
  });
}