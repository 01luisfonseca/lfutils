"use strict";

module.exports = base64ToFile;

function base64ToFile(dataURI) {
  var paramsFile = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
    name: 'file'
  };
  var byteString, mimestring;

  if (dataURI.split(',')[0].indexOf('base64') !== -1) {
    byteString = atob(dataURI.split(',')[1]);
  } else {
    byteString = decodeURI(dataURI.split(',')[1]);
  }

  mimestring = dataURI.split(',')[0].split(':')[1].split(';')[0];
  var content = new Array();

  for (var i = 0; i < byteString.length; i++) {
    content[i] = byteString.charCodeAt(i);
  }

  var newFile = new File([new Uint8Array(content)], paramsFile.name, {
    type: mimestring
  }); // Copy props set by the dropzone in the original file

  delete paramsFile.name;

  for (var key in paramsFile) {
    if (paramsFile.hasOwnProperty(key)) {
      newFile[key] = paramsFile[key];
    }
  }

  return newFile;
}