"use strict";

var _ = require('lodash');

var unitToSystem = require('./unitToSystem.function');

var binarySystemInvert = {
  '30': 'Gi',
  '20': 'Mi',
  '10': 'Ki',
  '0': '',
  '-10': 'mi',
  '-20': 'ui'
};
module.exports = unitToBinaryScale;

function unitToBinaryScale(config) {
  var number = _.get(config, 'number', 0);

  var unit = _.get(config, 'unit', '');

  return unitToSystem({
    number: number,
    unit: unit,
    system: binarySystemInvert,
    base: 2,
    expStep: 10
  });
}