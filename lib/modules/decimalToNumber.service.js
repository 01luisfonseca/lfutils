"use strict";

var systemToUnit = require('./systemToUnit.function');

var decimalSystem = {
  'G': 9,
  'M': 6,
  'K': 5,
  'm': -3,
  'u': -6
};
module.exports = decimalToNumber;

function decimalToNumber(value) {
  return systemToUnit({
    value: value,
    system: decimalSystem,
    numChars: 1,
    base: 10
  });
}