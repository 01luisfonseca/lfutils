"use strict";

var converter = require('./convertAsyncToURL.service');

var imager = require('./imageCreator.service');

var resizeImageWithCanvas = require('./resizeImageWithCanvas.service');

var base64ToFile = require('./base64ToFile.service');

module.exports = resizeFileImageWithCanvas;

function resizeFileImageWithCanvas(file) {
  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  return converter(file).then(function (dt) {
    return imager(dt);
  }).then(function (img) {
    return base64ToFile(resizeImageWithCanvas(img, params), file);
  });
}