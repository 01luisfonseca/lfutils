"use strict";

module.exports = convertAsyncToURL;

function convertAsyncToURL(data) {
  return new Promise(function (resolve, reject) {
    var reader = new FileReader();
    reader.readAsDataURL(data);

    reader.onloadend = function () {
      return resolve(reader.result);
    };

    reader.onerror = function (err) {
      return reject(err);
    };

    reader.onabort = function () {
      return reject(new Error('Abort URL convertion'));
    };
  });
}