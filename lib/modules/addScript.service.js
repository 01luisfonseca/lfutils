"use strict";

var ScriptsAdded = [];
module.exports = addScript;

function addScript(src) {
  return new Promise(function (resolve, reject) {
    var added = ScriptsAdded.find(function (x) {
      return x.src === src;
    });
    if (added) return resolve(added.event);
    var s = document.createElement('script');
    s.setAttribute('src', src);

    s.onload = function (event) {
      ScriptsAdded.push({
        src: src,
        event: event
      });
      setTimeout(function () {
        resolve(event);
      }, 50);
    };

    s.onerror = reject;
    s.onabort = reject;
    document.body.appendChild(s);
  });
}