"use strict";

module.exports = imgDomCacher;

function imgDomCacher() {
  return {
    setItem: function setItem(url) {
      return new Promise(function (resolve, reject) {
        var img = new Image();
        img.crossOrigin = 'anonymous';
        img.src = url;
        img.onabort = reject;
        img.onerror = reject;

        img.onload = function () {
          var canvas = document.createElement('canvas');
          canvas.width = img.width;
          canvas.height = img.height;
          console.log(canvas, img);
          var ctx = canvas.getContext('2d');
          ctx.drawImage(img, 0, 0);
          var imgBlob = canvas.toBlob(function () {
            localForage.setItem("get_".concat(url), imgBlob).then(function () {
              return resolve(imgBlob);
            })["catch"](reject);
          });
          canvas.onerror = reject;
          canvas.onerror = reject;
        };
      });
    },
    getItem: function getItem(url) {
      return localForage.getItem("get_".concat(url));
    }
  };
}