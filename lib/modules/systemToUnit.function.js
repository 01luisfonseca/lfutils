"use strict";

module.exports = systemToUnit;

function systemToUnit(config) {
  var result = 0;

  try {
    var number = config.value.match(/^\d+/);

    if (number && !!Number(number)) {
      var units = config.value.replace(number, '').trim();
      var constant = units !== '' && config.system && config.system[units.substr(0, config.numChars ? config.numChars : 1)] ? config.system[units.substr(0, config.numChars ? config.numChars : 1)] : 0;
      result = Number(number) * Math.pow(config.base ? config.base : 10, constant);
    }
  } catch (error) {
    console.error(error);
  }

  return result;
}