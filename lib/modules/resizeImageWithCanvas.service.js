"use strict";

module.exports = resizeImageWithCanvas;

function resizeImageWithCanvas(img) {
  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var MAX_WIDTH = params.width || 1200;
  var MAX_HEIGHT = params.height || 1200;
  var OUTPUT_QUALITY = params.quality || .7;
  var width = img.width;
  var height = img.height; // Don't resize if it's small enough

  if (width > MAX_WIDTH || height > MAX_HEIGHT) {
    if (width > height) {
      if (width > MAX_WIDTH) {
        height *= MAX_WIDTH / width;
        width = MAX_WIDTH;
      }
    } else {
      if (height > MAX_HEIGHT) {
        width *= MAX_HEIGHT / height;
        height = MAX_HEIGHT;
      }
    }
  }

  var canvas = document.createElement("canvas");
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  canvas.width = width;
  canvas.height = height;
  ctx.drawImage(img, 0, 0, width, height);

  if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
    return canvas.toDataURL("image/jpeg", OUTPUT_QUALITY);
  } else {
    return canvas.toDataURL("image/jpeg");
  }
}