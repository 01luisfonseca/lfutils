"use strict";

var _ = require('lodash');

var unitToSystem = require('./unitToSystem.function');

var decimalSystemInvert = {
  '9': 'G',
  '6': 'M',
  '3': 'K',
  '0': '',
  '-3': 'm',
  '-6': 'u'
};
module.exports = unitToDecimal;

function unitToDecimal(config) {
  var number = _.get(config, 'number', 0);

  var unit = _.get(config, 'unit', '');

  return unitToSystem({
    number: number,
    unit: unit,
    system: decimalSystemInvert,
    base: 10,
    expStep: 3
  });
}