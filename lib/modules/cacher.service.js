"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.cacher = void 0;

var requester = require('./requester.service');

var localForage = require('localforage');

var moment = require("moment"); // eslint-disable-next-line


var lf = localForagePromise(); // eslint-disable-next-line

var ls = localStorePromise();

function urlDetected(url) {
  var nameBucket, list, status, i, lst, dataToUpdate;
  return regeneratorRuntime.async(function urlDetected$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          if (typeof url === "string" && localStorage) {
            nameBucket = "lfutils_listOfCached";
            if (!localStorage.getItem(nameBucket)) localStorage.setItem(nameBucket, "[]");
            list = JSON.parse(localStorage.getItem(nameBucket));
            status = false; // checking existence of url cached. Update if finded

            for (i = 0; i < list.length; i++) {
              lst = list[i];

              if (lst.url === url && status === false) {
                status = true;
                lst.date = moment().format();
              }
            } // If not finded, added to list


            if (status === false) {
              list.push({
                url: url,
                date: moment().format()
              });
            }

            dataToUpdate = list.filter(function _callee(lst) {
              var mantainCache;
              return regeneratorRuntime.async(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      mantainCache = moment().diff(moment(lst.date), "d") < (_.get(window, "mediaCache") || 24);

                      if (mantainCache) {
                        _context.next = 4;
                        break;
                      }

                      _context.next = 4;
                      return regeneratorRuntime.awrap(localForage.removeItem(url));

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              });
            });
            localStorage.setItem(nameBucket, JSON.stringify(dataToUpdate));
          }

        case 1:
        case "end":
          return _context2.stop();
      }
    }
  });
}

var cacher = function cacher(baseURL, aditionalMark) {
  return {
    get: function get(endpoint, headers, cacheFirst, cacheSystem, objectToRequest) {
      return ajaxCacheator('get', endpoint, undefined, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest);
    },
    put: function put(endpoint, data, headers, cacheFirst, cacheSystem, objectToRequest) {
      return ajaxCacheator('put', endpoint, data, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest);
    },
    patch: function patch(endpoint, data, headers, cacheFirst, cacheSystem, objectToRequest) {
      return ajaxCacheator('parch', endpoint, data, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest);
    },
    post: function post(endpoint, data, headers, cacheFirst, cacheSystem, objectToRequest) {
      return ajaxCacheator('post', endpoint, data, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest);
    },
    "delete": function _delete(endpoint, headers, cacheFirst, cacheSystem, objectToRequest) {
      return ajaxCacheator('delete', endpoint, undefined, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest);
    },
    options: function options(endpoint, headers, cacheFirst, cacheSystem, objectToRequest) {
      return ajaxCacheator('options', endpoint, undefined, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest);
    },
    head: function head(endpoint, headers, cacheFirst, cacheSystem, objectToRequest) {
      return ajaxCacheator('head', endpoint, undefined, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest);
    }
  };
  /**
   * Request generator with cache for ajax XMLHTTPRequest
   * @param {String} method Method of request
   * @param {String} endpoint Enpoint of server
   * @param {Object} data Array or Object to send
   * @param {Object} headers Properties of request in server
   */

  function ajaxCacheator(method, endpoint, data, headers, aditionalMark, cacheFirst) {
    var cacheSystem = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : 'indexeddb';
    var objectToRequest = arguments.length > 7 ? arguments[7] : undefined;
    method = method.toLowerCase();
    cacheSystem = cacheSystem.toLowerCase();
    var cacheActuator = systemOfCache(cacheSystem);
    var stringOfCache = method + (aditionalMark && typeof aditionalMark === 'string' ? '_' + aditionalMark.replace(' ', '') : '') + '_' + baseURL + (endpoint ? endpoint.split('?')[0] : '');

    if (cacheFirst && method === 'get') {
      return cacheActuator.getItem(stringOfCache).then(function (cached) {
        console.log('Received cachefirst', stringOfCache);

        if (cached) {
          if (_.get(cacheFirst, "networkAttemp")) {
            requester(baseURL).manual(method, endpoint, data, headers, objectToRequest).then(function (dt) {
              return cacheActuator.setItem(stringOfCache, dt._body);
            })["catch"](function (err) {
              return console.log('Error in query. Not has results, but is resolved cache.', stringOfCache, err);
            });
          }

          return {
            _body: cached
          };
        } else {
          console.log('Error in cache, try to get with network');
          return requester(baseURL).manual(method, endpoint, data, headers, objectToRequest).then(function (dt) {
            cacheActuator.setItem(stringOfCache, dt._body);
            return dt;
          })["catch"](function (err) {
            return console.error('Error in query. Not has results', stringOfCache, err);
          });
        }
      });
    }

    return new Promise(function (resolve, reject) {
      return requester(baseURL).manual(method, endpoint, data, headers, objectToRequest).then(function (dt) {
        if (method === 'get') {
          console.log('Received networkfirst', stringOfCache);
          cacheActuator.setItem(stringOfCache, dt._body);
        }

        resolve(dt);
      })["catch"](function (err) {
        if (method === 'get') {
          console.log('Error in request, trying to get cache');
          cacheActuator.getItem(stringOfCache).then(function (cached) {
            if (cached) return resolve({
              _body: cached
            });
            console.error('Error in query. Not has results', stringOfCache, err);
            var reason = 'Not has results';
            reject(reason);
          });
        } else throw err;
      });
    });
  }
};
/**
 * Return instances of getItem and setItem, with promises
 * @param {String} type Type of method of cache
 */


exports.cacher = cacher;

function systemOfCache(type) {
  switch (type.toLowerCase()) {
    case 'localstorage':
      return ls;

    case 'indexeddb':
      return lf;

    default:
      var reason = 'Strategy not defined';
      return Promise.reject(reason);
  }
}
/**
 * Function to generate localStorage promisable
 */


function localStorePromise() {
  return {
    getItem: function getItem(key) {
      return Promise.resolve(localStorage.getItem(key));
    },
    setItem: function setItem(key, data) {
      return Promise.resolve(localStorage.setItem(key, data));
    }
  };
}
/**
 * Function to generate localForage
 */


function localForagePromise() {
  return {
    getItem: function getItem(key) {
      return localForage.getItem(key).then(function _callee2(dt) {
        return regeneratorRuntime.async(function _callee2$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return regeneratorRuntime.awrap(urlDetected(key));

              case 2:
                return _context3.abrupt("return", dt);

              case 3:
              case "end":
                return _context3.stop();
            }
          }
        });
      });
    },
    setItem: function setItem(key, data) {
      return localForage.setItem(key, data);
    }
  };
}