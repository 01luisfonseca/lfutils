"use strict";

var _ = require('lodash');

var WOF = require('./workerOnFly.service');

module.exports = PromisedWOF;

function PromisedWOF(func, conf) {
  return new Promise(function (resolve, reject) {
    var worker = WOF(func);
    worker.postMessage(conf);

    worker.onmessage = function (event) {
      return resolve(_.get(event, 'data'));
    };

    worker.onerror = reject;
  });
}