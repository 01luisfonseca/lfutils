"use strict";

var _ = require('lodash');

module.exports = xhr;

function xhr(request) {
  return new Promise(function (resolve, reject) {
    var formData = new FormData();
    var req = new XMLHttpRequest();
    if (_.get(request, 'responseType')) req.responseType = request.responseType;

    req.onreadystatechange = function () {
      if (req.readyState === 4) {
        if (req.status >= 200 && req.status <= 299) resolve({
          headers: req,
          _body: req.response
        });else reject(req.response);
      }
    };

    req.onerror = function (err) {
      return reject(err);
    };

    req.onabort = function () {
      return reject(null);
    };

    req.open(_.get(request, 'method', 'get').toUpperCase(), _.get(request, 'url', 'NO_URL'), true);

    var headers = _.get(request, 'headers', {});

    Object.keys(headers).forEach(function (key) {
      return req.setRequestHeader(key, headers[key]);
    });

    var body = _.get(request, 'data', _.get(request, 'body', {}));

    req.send(bodyComposer(headers, body, formData));
  });
}

;

function bodyComposer(headers, body, formData) {
  if (headers.hasOwnProperty('Content-Type')) {
    if (headers['Content-Type'].indexOf('json') > -1) return jsonConverter(body);
  }

  return formDataConverter(body, formData);
}

function formDataConverter(params, formData) {
  Object.keys(params).forEach(function (key) {
    if (params[key] !== undefined) {
      if (params[key] instanceof File) {
        formData.append(key, params[key], params[key].name);
      } else if (Array.isArray(params[key])) {
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = params[key][Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var elem = _step.value;
            formData.append(key, typeof elem === 'string' ? elem : JSON.stringify(elem));
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator["return"] != null) {
              _iterator["return"]();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }
      } else {
        formData.append(key, typeof params[key] === 'string' || typeof params[key] === 'number' ? params[key] : JSON.stringify(params[key]));
      }
    }
  });
  return formData;
}

function jsonConverter(params) {
  return JSON.stringify(params);
}