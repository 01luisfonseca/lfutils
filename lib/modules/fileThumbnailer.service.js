"use strict";

var resizeFileImageWithCanvas = require('./resizeFileImageWithCanvas.service');

var videoThumbnailer = require('./videoThumbnailer.service');

var generate = require('string-to-color');

var base64ToFile = require('./base64ToFile.service');

var _ = require('lodash');

module.exports = fileThumbnailer;

function fileThumbnailer(file) {
  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  if (file.type.indexOf('image') > -1) return resizeFileImageWithCanvas(file, Object.assign({
    height: 400,
    width: 400
  }, params));
  if (file.type.indexOf('video') > -1) return videoThumbnailer(URL.createObjectURL(file), file.name.split('.')[0], Object.assign({
    height: 400,
    width: 400
  }, params));
  var name = file.name.split('.');
  var ext = name.length > 1 ? name[name.length - 1].substring(0, 3) : name[0].substring(0, 3);
  var canvas = document.createElement("canvas");
  canvas.height = _.get(params, 'height', 400);
  canvas.width = _.get(params, 'width', 400);
  var ctx = canvas.getContext('2d');
  ctx.fillStyle = 'white';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.font = '40px Arial';
  ctx.textBaseline = 'middle';
  ctx.fillStyle = generate(ext.toLowerCase());
  ctx.textAlign = 'center';
  ctx.fillText('.' + ext.toLowerCase(), canvas.width / 2, canvas.height / 2);
  return resizeFileImageWithCanvas(base64ToFile(canvas.toDataURL(), {
    name: "".concat(name[0], ".png")
  }));
}