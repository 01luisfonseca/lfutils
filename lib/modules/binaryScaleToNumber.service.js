"use strict";

var systemToUnit = require('./systemToUnit.function');

var binarySystem = {
  'Gi': 30,
  'Mi': 20,
  'Ki': 10,
  'mi': -10,
  'ui': -20
};
module.exports = decimalToNumber;

function decimalToNumber(value) {
  return systemToUnit({
    value: value,
    system: binarySystem,
    numChars: 2,
    base: 2
  });
}