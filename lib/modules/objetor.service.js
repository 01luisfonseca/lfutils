"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Objetor = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Objetor =
/*#__PURE__*/
function () {
  function Objetor(fb, selector, reference, parentId) {
    _classCallCheck(this, Objetor);

    this.__$parent = null;
    this.__$parentId = null;
    this.__$selector = null;
    this.__$listUnsuscriptor = null;
    this.__$readUnsuscriptor = null;
    if (!fb || !fb.firestoreWithPersistence) throw new Error('Firebase of lfutils is required');
    this.__$parent = reference;
    this.__$selector = selector;
    this.__$parentId = parentId;
  }

  _createClass(Objetor, [{
    key: "__$reference",
    value: function __$reference() {
      var $ref;
      return regeneratorRuntime.async(function __$reference$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!(this.__$parent && this.__$parentId)) {
                _context.next = 6;
                break;
              }

              _context.next = 3;
              return regeneratorRuntime.awrap(this.__$parent.__$doc(this.__$parentId));

            case 3:
              _context.t0 = _context.sent;
              _context.next = 9;
              break;

            case 6:
              _context.next = 8;
              return regeneratorRuntime.awrap(fb.firestoreWithPersistence());

            case 8:
              _context.t0 = _context.sent;

            case 9:
              $ref = _context.t0;
              return _context.abrupt("return", $ref);

            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "__$queryCalculator",
    value: function __$queryCalculator(fbElm, query) {
      // Recursive cicle in array
      // .orderBy(finalQuery.field, finalQuery.order)
      // .where(finalquery.field, finalquery.op, finalquery.value)
      // .startAt(finalQuery.row)
      // .limit(finalQuery.limit)
      if (query && _lodash["default"].get(query, '[0].action', false)) {
        var toQuery = query.shift();

        switch (toQuery.action) {
          case 'orderBy':
            fbElm = toQuery.field ? fbElm[toQuery.action](toQuery.field, _lodash["default"].get(toQuery, 'order', 'asc')) : fbElm;
            break;

          case 'where':
            fbElm = toQuery.field && toQuery.op && toQuery.value ? fbElm[toQuery.action](toQuery.field, toQuery.op, toQuery.value) : fbElm;
            break;

          case 'startAt':
          case 'limit':
            fbElm = toQuery.row ? fbElm[toQuery.action](toQuery.row) : fbElm;
            break;
        }

        return this.__$queryCalculator(fbElm, query);
      }

      return fbElm;
    }
  }, {
    key: "__$collection",
    value: function __$collection() {
      return regeneratorRuntime.async(function __$collection$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return regeneratorRuntime.awrap(this.__$reference());

            case 2:
              _context2.t0 = this.__$selector;
              return _context2.abrupt("return", _context2.sent.collection(_context2.t0));

            case 4:
            case "end":
              return _context2.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "__$doc",
    value: function __$doc(uid) {
      return regeneratorRuntime.async(function __$doc$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return regeneratorRuntime.awrap(this.__$collection());

            case 2:
              _context3.t0 = uid;
              return _context3.abrupt("return", _context3.sent.doc(_context3.t0));

            case 4:
            case "end":
              return _context3.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "get",
    value: function get(uid) {
      return regeneratorRuntime.async(function get$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return regeneratorRuntime.awrap(this.__$doc(uid));

            case 2:
              return _context4.abrupt("return", _context4.sent.get());

            case 3:
            case "end":
              return _context4.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "list",
    value: function list(query) {
      var fbElm;
      return regeneratorRuntime.async(function list$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.t0 = this;
              _context5.next = 3;
              return regeneratorRuntime.awrap(this.__$collection());

            case 3:
              _context5.t1 = _context5.sent;
              _context5.t2 = query;
              fbElm = _context5.t0.__$queryCalculator.call(_context5.t0, _context5.t1, _context5.t2);
              return _context5.abrupt("return", fbElm.get().then(function (docs) {
                return docs.docs.map(function (doc) {
                  return Object.assign({
                    id: doc.id
                  }, doc.data());
                });
              }));

            case 7:
            case "end":
              return _context5.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "listlive",
    value: function listlive(cb, query) {
      return regeneratorRuntime.async(function listlive$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              _context6.next = 2;
              return regeneratorRuntime.awrap(this.__$collection());

            case 2:
              _context6.t0 = function (snapshot) {
                var toResult = [];
                snapshot.forEach(function (doc) {
                  return toResult.push(_objectSpread({
                    id: doc.id
                  }, doc.data()));
                });
                cb(toResult);
              };

              this.__$listUnsuscriptor = _context6.sent.onSnapshot(_context6.t0);

            case 4:
            case "end":
              return _context6.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "listCloseLive",
    value: function listCloseLive() {
      if (this.__$listUnsuscriptor) this.__$listUnsuscriptor();
    }
  }, {
    key: "read",
    value: function read(uid) {
      return this.get(uid).then(function (doc) {
        return doc.exists ? Object.assign({
          id: doc.id
        }, doc.data()) : null;
      });
    }
  }, {
    key: "readlive",
    value: function readlive(uid, cb) {
      return regeneratorRuntime.async(function readlive$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              _context7.next = 2;
              return regeneratorRuntime.awrap(this.__$doc(uid));

            case 2:
              _context7.t0 = function (doc) {
                return cb(Object.assign({
                  id: doc.id
                }, doc.data()));
              };

              this.$readUnsuscriptor = _context7.sent.onSnapshot(_context7.t0);

            case 4:
            case "end":
              return _context7.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "readCloseLive",
    value: function readCloseLive() {
      if (this.$readUnsuscriptor) this.$readUnsuscriptor();
    }
  }, {
    key: "create",
    value: function create(data) {
      return regeneratorRuntime.async(function create$(_context8) {
        while (1) {
          switch (_context8.prev = _context8.next) {
            case 0:
              _context8.next = 2;
              return regeneratorRuntime.awrap(this.__$collection());

            case 2:
              _context8.t0 = Object.assign(data, {
                createdAt: Date.now(),
                updatedAt: Date.now()
              });
              return _context8.abrupt("return", _context8.sent.add(_context8.t0));

            case 4:
            case "end":
              return _context8.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "update",
    value: function update(uid, data) {
      return regeneratorRuntime.async(function update$(_context9) {
        while (1) {
          switch (_context9.prev = _context9.next) {
            case 0:
              if (data) {
                _context9.next = 2;
                break;
              }

              return _context9.abrupt("return");

            case 2:
              if (data.id) delete data.id;
              _context9.next = 5;
              return regeneratorRuntime.awrap(this.__$doc(uid));

            case 5:
              _context9.t0 = Object.assign({}, data, {
                updatedAt: Date.now()
              });
              return _context9.abrupt("return", _context9.sent.update(_context9.t0));

            case 7:
            case "end":
              return _context9.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "delete",
    value: function _delete(uid) {
      return regeneratorRuntime.async(function _delete$(_context10) {
        while (1) {
          switch (_context10.prev = _context10.next) {
            case 0:
              _context10.next = 2;
              return regeneratorRuntime.awrap(this.__$doc(uid));

            case 2:
              return _context10.abrupt("return", _context10.sent["delete"]());

            case 3:
            case "end":
              return _context10.stop();
          }
        }
      }, null, this);
    }
  }]);

  return Objetor;
}();

exports.Objetor = Objetor;