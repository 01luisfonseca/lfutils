"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Firebase = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

// Firebase App is always required and must be first
var firebase = require("firebase/app"); // Add additional services that you want to use


require("firebase/auth");

require("firebase/database");

require("firebase/firestore");

require("firebase/messaging");

require("firebase/functions");

require("firebase/storage");

var Firebase =
/*#__PURE__*/
function () {
  function Firebase(config) {
    _classCallCheck(this, Firebase);

    if (!firebase.apps.length) firebase.initializeApp(config);else firebase.app();
    this.$fb = firebase;
    this.$persistence = false;
    var firestore = firebase.firestore(); // var settings = { timestampsInSnapshots: true }
    // firestore.settings(settings)

    this.$fs = firestore;
  }

  _createClass(Firebase, [{
    key: "firestoreWithPersistence",
    value: function firestoreWithPersistence() {
      return regeneratorRuntime.async(function firestoreWithPersistence$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (window.fsPersistence) {
                _context.next = 5;
                break;
              }

              _context.next = 3;
              return regeneratorRuntime.awrap(this.$fs.enablePersistence());

            case 3:
              window.fsPersistence = true;
              this.$persistence = true;

            case 5:
              return _context.abrupt("return", this.$fs);

            case 6:
            case "end":
              return _context.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "auth",
    value: function auth() {
      return this.$fb.auth();
    }
  }]);

  return Firebase;
}();

exports.Firebase = Firebase;