"use strict";

module.exports = imageCreator;

function imageCreator(url) {
  return new Promise(function (resolve, reject) {
    var origImg = new Image();
    origImg.src = url;
    origImg.addEventListener("load", function () {
      return resolve(origImg);
    });

    origImg.onabort = function () {
      return reject('Abort Image');
    };

    origImg.onerror = reject;
  });
}