"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _ = require('lodash');

module.exports = {
  log: function log() {
    return console.log.apply(this, arguments);
  },
  error: function error() {
    return console.error.apply(this, arguments);
  },
  objetor: function objetor(objetive) {
    return objetive && _typeof(objetive) === 'object' ? Object.assign({}, objetive) : {};
  },
  checksum8bitXor: function checksum8bitXor(str) {
    return _toConsumableArray(str).map(function (a) {
      return a.charCodeAt(0);
    }).reduce(function (a, b) {
      return a ^ b;
    });
  },
  getRandomInt: function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  },
  getRandomRGB: function getRandomRGB() {
    var color = 'rgb(';
    color += getRandomInt(0, 255) + ', ';
    color += getRandomInt(0, 255) + ', ';
    color += getRandomInt(0, 255);
    color += ')';
    return color;
  },
  validMail: function validMail(email) {
    var regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regexp.test(email);
  },
  Timer: require('./modules/centralTimer'),
  cacher: require('./modules/cacher.service'),
  firebaseObject: require('./modules/objetor.service'),
  firebase: require('./modules/firebase.service'),
  requester: require('./modules/requester.service'),
  fileThumbnailer: require('./modules/fileThumbnailer.service'),
  resizeFileImageWithCanvas: require('./modules/resizeFileImageWithCanvas.service'),
  resizeImageWithCanvas: require('./modules/resizeImageWithCanvas.service'),
  promisedImageCreator: require('./modules/imageCreator.service'),
  convertAsyncToURL: require('./modules/convertAsyncToURL.service'),
  addScript: require('./modules/addScript.service'),
  unitToDecimal: require('./modules/unitToDecimal.service'),
  decimalToNumber: require('./modules/decimalToNumber.service'),
  unitToBinaryScale: require('./modules/unitToBinaryScale.service'),
  binaryScaleToNumber: require('./modules/binaryScaleToNumber.service'),
  isMobile: require('./modules/evalmobile.service'),
  workerOnFly: require('./modules/workerOnFly.service'),
  PromisedWorkerOnFly: require('./modules/PromisedWOF.service'),
  xhrPromiser: require('./modules/xhr.service'),
  imgDomCacher: require('./modules/ImgDomCacher.service'),
  copyToClipboard: require('./modules/copyClip.service')
};