const _ = require('lodash')

module.exports = {
  log () { return console.log.apply(this, arguments) },
  error () { return console.error.apply(this, arguments) },
  objetor(objetive){ return objetive && typeof objetive === 'object' ? Object.assign({}, objetive) : {}; },

  checksum8bitXor (str) { return [...str].map(a => a.charCodeAt(0)).reduce((a, b) => a ^ b); },

  getRandomInt (min, max) { return Math.floor(Math.random() * (max - min + 1) + min) },

  getRandomRGB () {
    var color = 'rgb(';
    color += getRandomInt(0, 255) + ', ';
    color += getRandomInt(0, 255) + ', ';
    color += getRandomInt(0, 255);
    color += ')';
    return color;
  },

  validMail (email) {
    const regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return regexp.test(email)
  },

  Timer: require('./modules/centralTimer'),

  cacher: require('./modules/cacher.service'),

  firebaseObject: require('./modules/objetor.service'),
  firebase: require('./modules/firebase.service'),

  requester: require('./modules/requester.service'),

  fileThumbnailer: require('./modules/fileThumbnailer.service'),
  resizeFileImageWithCanvas: require('./modules/resizeFileImageWithCanvas.service'),
  resizeImageWithCanvas: require('./modules/resizeImageWithCanvas.service'),
  promisedImageCreator: require('./modules/imageCreator.service'),

  convertAsyncToURL: require('./modules/convertAsyncToURL.service'),

  addScript: require('./modules/addScript.service'),

  unitToDecimal: require('./modules/unitToDecimal.service'),
  decimalToNumber: require('./modules/decimalToNumber.service'),

  unitToBinaryScale: require('./modules/unitToBinaryScale.service'),
  binaryScaleToNumber: require('./modules/binaryScaleToNumber.service'),

  isMobile: require('./modules/evalmobile.service'),

  workerOnFly: require('./modules/workerOnFly.service'),

  PromisedWorkerOnFly: require('./modules/PromisedWOF.service'),

  xhrPromiser: require('./modules/xhr.service'),

  imgDomCacher: require('./modules/ImgDomCacher.service'),

  copyToClipboard: require('./modules/copyClip.service')
}
