module.exports = systemToUnit;

function systemToUnit(config) {
  let result = 0;
  try {
    let number = config.value.match(/^\d+/);
    if (number && !!Number(number)) {
      let units = config.value.replace(number, '').trim();
      let constant = units !== '' && config.system &&
        config.system[units.substr(0,(config.numChars ? config.numChars : 1))] ? 
        config.system[units.substr(0,(config.numChars ? config.numChars : 1))] : 0;
      result = Number(number) * Math.pow(config.base ? config.base : 10, constant);
    }
  } catch (error) {
    console.error(error);
  }
  return result;
}
