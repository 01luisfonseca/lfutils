const Exif = require('@fengyuanchen/exif');
const localForage = require('localforage')

function imageRotator(file, resolveFile) {
  return new Promise((resolve, reject) => {
    new Exif(file, {
      done: (tags) => {
        this.dataToURL(file).then(
          info => {
            const orientation = tags.Orientation;
            const img = document.createElement('img');
            const canvas = document.createElement('canvas');
            img.src = info.url;
            img.onload = () => {
              transformOrientation(img, canvas, orientation);
              canvas.toBlob(blob => {
                const staticFile = new File([blob], file.name, {
                  type: 'image/jpeg',
                });
                if (resolveFile) resolve(staticFile)
                else this.dataToURL(staticFile).then(resolve, reject);
              }, 'image/jpeg', 1);
            };
          }
        );
      },
      fail: (message) => {
        console.error(message)
        console.log('Image not require transform');
        if (resolveFile) resolve(file)
        else this.dataToURL(file).then(resolve).catch(reject);
      }
    });
  });
}

function transformOrientation(img, canvas, orientation) {
  // Set variables
  const ctx = canvas.getContext('2d');
  const width = img.width, height = img.height;
  // set proper canvas dimensions before transform & export
  if (orientation >= 5) {
      canvas.width = height;
      canvas.height = width;
  } else {
      canvas.width = width;
      canvas.height = height;
  }
  // transform context before drawing image
  switch (orientation) {
      case 2:
          ctx.transform(-1, 0, 0, 1, width, 0);
          break;
      case 3:
          ctx.transform(-1, 0, 0, -1, width, height);
          break;
      case 4:
          ctx.transform(1, 0, 0, -1, 0, height);
          break;
      case 5:
          ctx.transform(0, 1, 1, 0, 0, 0);
          break;
      case 6:
          ctx.transform(0, 1, -1, 0, height, 0);
          break;
      case 7:
          ctx.transform(0, -1, -1, 0, height, width);
          break;
      case 8:
          ctx.transform(0, -1, 1, 0, 0, width);
          break;
      default:
          ctx.transform(1, 0, 0, 1, 0, 0);
  }
  // Draw img into canvas
  ctx.drawImage(img, 0, 0, width, height);
}
