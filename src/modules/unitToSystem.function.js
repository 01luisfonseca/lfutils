module.exports = unitToSystem

function unitToSystem(config, acum = 0) {
  const unit = config.unit || ''
  try {
    if (typeof config.number !== 'number') return '0';
    return config.number < Math.pow(config.base, config.expStep) ? 
      `${config.number} ${config.system && config.system[acum] ? config.system[acum] : ''}${unit}` : 
      unitToSystem(Object.assign(
        config,
        {number: Number((config.number/Math.pow(config.base, config.expStep)).toFixed(2))}
      ), acum + config.expStep);      
  } catch (error) {
    console.error(error);
    return '0';
  }
}
