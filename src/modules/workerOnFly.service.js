module.exports = {
  setBlobUrl (fnc) {
    if (typeof fnc === 'function') {
      return URL.createObjectURL(new Blob([ '(', fnc.toString(), ')()' ], { type: 'application/javascript' }))
    }
  },
  setWorker (fnc) {
    let url = this.setBlobUrl(fnc)
    if (url) {
      let worker = new Worker(url)
      URL.revokeObjectURL(url)
      return worker
    }
  }
}
