const _ = require('lodash')
const resizeFileImageWithCanvas = require('./resizeFileImageWithCanvas.service');
const base64ToFile = require('./base64ToFile.service');

module.exports = videoThumbnailer;

function videoThumbnailer (videoB64, name, params) {
  const secondOfThumbnail = _.get(params, 'secondOfThumbnail', 3)

  return new Promise((resolve, reject) => {
    if (typeof videoB64 !== 'string') throw new Error('Video source param is not string')
    const videoTag = document.createElement('video')
    let canvas = document.createElement("canvas")

    videoTag.src = videoB64
    videoTag.addEventListener('loadedmetadata', () => {
      canvas.width = videoTag.videoWidth
      canvas.height = videoTag.videoHeight
      videoTag.currentTime = secondOfThumbnail
    })
    videoTag.addEventListener('timeupdate', () => {
      name = `${name}.png`
      const ctx = canvas.getContext('2d')
      ctx.drawImage(videoTag, 0, 0, videoTag.videoWidth, videoTag.videoHeight)
      resizeFileImageWithCanvas(base64ToFile(canvas.toDataURL(), { name }), Object.assign({ height: 400, width: 400 }, params)).then(resolve).catch(reject)
    })
  })
}
