const converter = require('./convertAsyncToURL.service');
const imager = require('./imageCreator.service');
const resizeImageWithCanvas = require('./resizeImageWithCanvas.service');
const base64ToFile = require('./base64ToFile.service')

module.exports = resizeFileImageWithCanvas

function resizeFileImageWithCanvas (file, params = {}) {
  return converter(file)
    .then(dt => imager(dt))
    .then(img => base64ToFile(resizeImageWithCanvas(img, params), file));
}
