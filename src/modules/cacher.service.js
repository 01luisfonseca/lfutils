const requester = require('./requester.service');
const localForage = require('localforage');
const moment = require("moment");

// eslint-disable-next-line
const lf = localForagePromise()
// eslint-disable-next-line
const ls = localStorePromise()

async function urlDetected(url) {
  if (typeof url === "string" && localStorage) {
    let nameBucket = "lfutils_listOfCached"
    if (!localStorage.getItem(nameBucket))
      localStorage.setItem(nameBucket, "[]");
    const list = JSON.parse(localStorage.getItem(nameBucket));
    let status = false;

    // checking existence of url cached. Update if finded
    for (let i = 0; i < list.length; i++) {
      const lst = list[i];
      if (lst.url === url && status === false) {
        status = true;
        lst.date = moment().format();
      }
    }

    // If not finded, added to list
    if (status === false) {
      list.push({
        url,
        date: moment().format()
      });
    }
    const dataToUpdate = list.filter(async lst => {
      const mantainCache =
        moment().diff(moment(lst.date), "d") <
        (_.get(window, "mediaCache") || 24);
      if (!mantainCache) {
        await localForage.removeItem(url);
      }
    });
    localStorage.setItem(
      nameBucket,
      JSON.stringify(dataToUpdate)
    );
  }
}

export let cacher = (baseURL, aditionalMark) => {
  return {
    get: (endpoint, headers, cacheFirst, cacheSystem, objectToRequest) => { return ajaxCacheator('get', endpoint, undefined, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest) },
    put: (endpoint, data, headers, cacheFirst, cacheSystem, objectToRequest) => { return ajaxCacheator('put', endpoint, data, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest) },
    patch: (endpoint, data, headers, cacheFirst, cacheSystem, objectToRequest) => { return ajaxCacheator('parch', endpoint, data, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest) },
    post: (endpoint, data, headers, cacheFirst, cacheSystem, objectToRequest) => { return ajaxCacheator('post', endpoint, data, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest) },
    delete: (endpoint, headers, cacheFirst, cacheSystem, objectToRequest) => { return ajaxCacheator('delete', endpoint, undefined, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest) },
    options: (endpoint, headers, cacheFirst, cacheSystem, objectToRequest) => { return ajaxCacheator('options', endpoint, undefined, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest) },
    head: (endpoint, headers, cacheFirst, cacheSystem, objectToRequest) => { return ajaxCacheator('head', endpoint, undefined, headers, aditionalMark, cacheFirst, cacheSystem, objectToRequest) }
  }

  /**
   * Request generator with cache for ajax XMLHTTPRequest
   * @param {String} method Method of request
   * @param {String} endpoint Enpoint of server
   * @param {Object} data Array or Object to send
   * @param {Object} headers Properties of request in server
   */
  function ajaxCacheator (method, endpoint, data, headers, aditionalMark, cacheFirst, cacheSystem = 'indexeddb', objectToRequest) {
    method = method.toLowerCase()
    cacheSystem = cacheSystem.toLowerCase()
    const cacheActuator = systemOfCache(cacheSystem)
    const stringOfCache =
      method +
      (aditionalMark && typeof aditionalMark === 'string' ? ('_' + aditionalMark.replace(' ', '')) : '') +
      '_' + baseURL +
      (endpoint ? endpoint.split('?')[0] : '')
    if (cacheFirst && method === 'get') {
      return cacheActuator.getItem(stringOfCache).then(cached => {
        console.log('Received cachefirst', stringOfCache)
        if (cached) {
          if (_.get(cacheFirst, "networkAttemp")) {
            requester(baseURL).manual(method, endpoint, data, headers, objectToRequest)
              .then((dt) => cacheActuator.setItem(stringOfCache, dt._body))
              .catch((err) => console.log('Error in query. Not has results, but is resolved cache.', stringOfCache, err))
          }
          return { _body: cached }
        } else {
          console.log('Error in cache, try to get with network')
          return requester(baseURL).manual(method, endpoint, data, headers, objectToRequest)
            .then((dt) => {
              cacheActuator.setItem(stringOfCache, dt._body)
              return dt
            })
            .catch((err) => console.error('Error in query. Not has results', stringOfCache, err))
        }
      })
    }
    return new Promise((resolve, reject) => {
      return requester(baseURL).manual(method, endpoint, data, headers, objectToRequest)
        .then((dt) => {
          if (method === 'get') {
            console.log('Received networkfirst', stringOfCache)
            cacheActuator.setItem(stringOfCache, dt._body)
          }
          resolve(dt)
        })
        .catch((err) => {
          if (method === 'get') {
            console.log('Error in request, trying to get cache')
            cacheActuator.getItem(stringOfCache).then(cached => {
              if (cached) return resolve({ _body: cached })
              console.error('Error in query. Not has results', stringOfCache, err)
              const reason = 'Not has results'
              reject(reason)
            })
          } else throw err
        })
    })
  }
}

/**
 * Return instances of getItem and setItem, with promises
 * @param {String} type Type of method of cache
 */
function systemOfCache (type) {
  switch (type.toLowerCase()) {
    case 'localstorage':
      return ls

    case 'indexeddb':
      return lf

    default:
      const reason = 'Strategy not defined'
      return Promise.reject(reason)
  }
}

/**
 * Function to generate localStorage promisable
 */
function localStorePromise () {
  return {
    getItem: (key) => Promise.resolve(localStorage.getItem(key)),
    setItem: (key, data) => Promise.resolve(localStorage.setItem(key, data))
  }
}

/**
 * Function to generate localForage
 */
function localForagePromise () {
  return {
    getItem: key => {
      return localForage.getItem(key).then(async dt => {
        await urlDetected(key);
        return dt;
      });
    },
    setItem: (key, data) => localForage.setItem(key, data)
  }
}
