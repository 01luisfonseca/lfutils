const _ = require('lodash')
const unitToSystem = require('./unitToSystem.function');
const decimalSystemInvert= {
  '9': 'G',
  '6': 'M',
  '3': 'K',
  '0': '',
  '-3':'m',
  '-6':'u'
};

module.exports = unitToDecimal

function unitToDecimal(config) {
  const number = _.get(config, 'number', 0);
  const unit = _.get(config, 'unit', '');
  return unitToSystem({
    number,
    unit,
    system: decimalSystemInvert,
    base: 10,
    expStep: 3
  });
}