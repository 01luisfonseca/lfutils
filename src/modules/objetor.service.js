import _ from 'lodash'

export class Objetor {
  constructor (fb, selector, reference, parentId) {
    this.__$parent = null
    this.__$parentId = null
    this.__$selector = null
    this.__$listUnsuscriptor = null
    this.__$readUnsuscriptor = null
    if (!fb || !fb.firestoreWithPersistence) throw new Error('Firebase of lfutils is required')
    this.__$parent = reference
    this.__$selector = selector
    this.__$parentId = parentId
  }

  async __$reference () {
    let $ref = this.__$parent && this.__$parentId ? await this.__$parent.__$doc(this.__$parentId) : await fb.firestoreWithPersistence()
    return $ref
  }

  __$queryCalculator (fbElm, query) { // Recursive cicle in array
    // .orderBy(finalQuery.field, finalQuery.order)
    // .where(finalquery.field, finalquery.op, finalquery.value)
    // .startAt(finalQuery.row)
    // .limit(finalQuery.limit)
    if (query && _.get(query, '[0].action', false)) {
      const toQuery = query.shift()
      switch (toQuery.action) {
        case 'orderBy':
          fbElm = toQuery.field ? fbElm[toQuery.action](toQuery.field, _.get(toQuery, 'order', 'asc')) : fbElm
          break

        case 'where':
          fbElm = toQuery.field && toQuery.op && toQuery.value ? fbElm[toQuery.action](toQuery.field, toQuery.op, toQuery.value) : fbElm
          break

        case 'startAt':
        case 'limit':
          fbElm = toQuery.row ? fbElm[toQuery.action](toQuery.row) : fbElm
          break
      }
      return this.__$queryCalculator(fbElm, query)
    }
    return fbElm
  }

  async __$collection () { return (await this.__$reference()).collection(this.__$selector) }

  async __$doc (uid) { return (await this.__$collection()).doc(uid) }

  async get (uid) { return (await this.__$doc(uid)).get() }

  async list (query) {
    let fbElm = this.__$queryCalculator(await this.__$collection(), query)
    return fbElm.get().then(docs => docs.docs.map(doc => Object.assign({ id: doc.id }, doc.data())))
  }

  async listlive (cb, query) {
    this.__$listUnsuscriptor = (await this.__$collection()).onSnapshot(snapshot => {
      const toResult = []
      snapshot.forEach(doc => toResult.push({ id: doc.id, ...doc.data() }))
      cb(toResult)
    })
  }

  listCloseLive () { if (this.__$listUnsuscriptor) this.__$listUnsuscriptor() }

  read (uid) { return this.get(uid).then(doc => (doc.exists ? Object.assign({ id: doc.id }, doc.data()) : null)) }

  async readlive (uid, cb) { this.$readUnsuscriptor = (await this.__$doc(uid)).onSnapshot(doc => cb(Object.assign({ id: doc.id }, doc.data()))) }

  readCloseLive () { if (this.$readUnsuscriptor) this.$readUnsuscriptor() }

  async create (data) { return (await this.__$collection()).add(Object.assign(data, { createdAt: Date.now(), updatedAt: Date.now() })) }

  async update (uid, data) {
    if (!data) return
    if (data.id) delete data.id
    return (await this.__$doc(uid)).update(Object.assign({}, data, { updatedAt: Date.now() }))
  }

  async delete (uid) { return (await this.__$doc(uid)).delete() }
}
