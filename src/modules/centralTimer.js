var timers = {
  timerID: undefined,
  timers: [],
  code:1,
  add: function(fn, time, repeat) {
    var assignedCode = ++this.code;
    this.timers.push({
      fn: fn,
      time: time,
      repeat: repeat,
      code: assignedCode,
      lastTime: Date.now(),
      remove: false
    });
    return assignedCode;
  },
  remove: function(code) {
    if (!code) return false;
    var encountered = false;
    for (let i = 0; i < this.timers.length; i++) {
      if (this.timers[i].code === code) {
        this.timers[i].remove = true;
        encountered = true;
      }
    }
    return encountered;
  },
  initModule: function() {
    if (this.timerID) return;
    (function runNext() {
      for (var i = 0; i < timers.timers.length; i++) {
        var timed = timers.timers[i];
        if (
          (Date.now() - timed.lastTime > (!!timed.time ? timed.time : 0)) || timed.remove
        ) {
          // console.log(timed.code, Date.now() - timed.lastTime, (!!timed.time ? timed.time : 0), timed.remove)
          timed.lastTime = Date.now();
          if (timed.remove) {
            timers.timers.splice(i, 1);
            i--;
          } else if (timed.fn.call() === false || !(!!timed.repeat)) { // TODO: Revisar call porque parece venenoso
            timers.timers.splice(i, 1);
            i--;
          }
        }        
      }
      timers.timerID = setTimeout(runNext, 0);
    })()
  },
  stopModule: function() {
    clearTimeout(this.timerID);
    this.timerID = undefined;
  }
};

export let Timer = timers;