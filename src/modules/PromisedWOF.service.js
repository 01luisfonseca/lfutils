const _  = require('lodash')
const WOF = require('./workerOnFly.service')

module.exports = PromisedWOF

function PromisedWOF (func, conf) {
  return new Promise((resolve, reject) => {
    const worker = WOF(func)
    worker.postMessage(conf)
    worker.onmessage = event => resolve(_.get(event, 'data'))
    worker.onerror = reject
  })
}
