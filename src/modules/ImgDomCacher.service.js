module.exports = imgDomCacher;

function imgDomCacher () {
  return {
    setItem (url) {
      return new Promise((resolve, reject) => {
        const img = new Image()
        img.crossOrigin = 'anonymous'
        img.src = url
        img.onabort = reject
        img.onerror = reject
        img.onload = () => {
          const canvas = document.createElement('canvas')
          canvas.width = img.width
          canvas.height = img.height
          console.log(canvas, img)
          const ctx = canvas.getContext('2d')
          ctx.drawImage(img, 0, 0)
          const imgBlob = canvas.toBlob(() => {
            localForage.setItem(`get_${url}`, imgBlob).then(
              () => resolve(imgBlob)
            ).catch(reject)
          })
          canvas.onerror = reject
          canvas.onerror = reject
        }
      })
    },
    getItem (url) { return localForage.getItem(`get_${url}`) }
  }
}