const _ = require('lodash')

module.exports = xhr

function xhr (request) {
  return new Promise((resolve, reject) => {
    const formData = new FormData();
    const req = new XMLHttpRequest();
    if (_.get(request, 'responseType')) req.responseType = request.responseType
    req.onreadystatechange = () => {
      if (req.readyState === 4) {
        if (req.status >= 200 && req.status <= 299) resolve({headers: req, _body: req.response});
        else reject(req.response);
      }
    };
    req.onerror = (err) => reject(err);
    req.onabort = () => reject(null);
    req.open(_.get(request, 'method', 'get').toUpperCase(), _.get(request, 'url', 'NO_URL'), true);
    let headers = _.get(request, 'headers', {});
    Object.keys(headers).forEach(key => req.setRequestHeader(key, headers[key]));
    const body = _.get(request, 'data', _.get(request, 'body', {}));
    req.send(bodyComposer(headers, body, formData));
  });
};

function bodyComposer(headers, body, formData) {
  if (headers.hasOwnProperty('Content-Type')) {
    if (headers['Content-Type'].indexOf('json') > -1) return jsonConverter(body);
  }
  return formDataConverter(body, formData);
}

function formDataConverter(params, formData) {
  Object.keys(params).forEach(key => {
    if (params[key] !== undefined) {
      if (params[key] instanceof File) {
        formData.append(key, params[key], params[key].name);
      } else if (Array.isArray(params[key])) {
        for (const elem of params[key]) {
          formData.append(key, typeof elem === 'string' ? elem : JSON.stringify(elem));
        }
      } else {
        formData.append(key, typeof params[key] === 'string' || typeof params[key] === 'number' ? params[key] : JSON.stringify(params[key]));
      }
    }
  });
  return formData;
}

function jsonConverter(params) {
  return JSON.stringify(params);
}
