const resizeFileImageWithCanvas = require('./resizeFileImageWithCanvas.service');
const videoThumbnailer = require('./videoThumbnailer.service');
const generate = require('string-to-color');
const base64ToFile = require('./base64ToFile.service');
const _ = require('lodash')

module.exports = fileThumbnailer;

function fileThumbnailer (file, params = {}) {
  if (file.type.indexOf('image') > -1) return resizeFileImageWithCanvas(file, Object.assign({ height: 400, width: 400 }, params))
  if (file.type.indexOf('video') > -1) return videoThumbnailer(URL.createObjectURL(file), file.name.split('.')[0], Object.assign({ height: 400, width: 400 }, params))
  const name = file.name.split('.')
  const ext = name.length > 1 ? name[name.length - 1].substring(0, 3) : name[0].substring(0, 3)
  let canvas = document.createElement("canvas")
  canvas.height = _.get(params, 'height', 400)
  canvas.width = _.get(params, 'width', 400)
  let ctx = canvas.getContext('2d')
  ctx.fillStyle = 'white'
  ctx.fillRect(0, 0, canvas.width, canvas.height)
  ctx.font = '40px Arial'
  ctx.textBaseline = 'middle'
  ctx.fillStyle = generate(ext.toLowerCase())
  ctx.textAlign = 'center'
  ctx.fillText('.' + ext.toLowerCase(), canvas.width/2, canvas.height/2)
  return resizeFileImageWithCanvas(base64ToFile(canvas.toDataURL(), { name: `${name[0]}.png` }))
}
