let waittime = 10000 // In millis
const requestCached = []
const xhr = require('./xhr.service');

module.exports = requester

function requester (baseUrl, config) {
  waittime = _.get(config, 'waitTime', waittime);
  var generalConfig = {
    baseUrl
  }

  return {
    get: (endpoint, headers) => new Promise((resolve, reject) => {
      let requestCache = requestCached.find(req => req.url === endpoint)
      const dataResolved = requestCache ? (requestCache.data && Date.now() - requestCache.timeStamp < waittime ? requestCache.data : null) : null
      if (!requestCache) { // Create new cache if not exist
        if (endpoint.charAt(0) === '/' && baseUrl.charAt(baseUrl.length - 1) === '/') endpoint = endpoint.substr(1)
        requestCache = {
          url: baseUrl + endpoint,
          data: null,
          timeStamp: Date.now(),
          inProgress: false,
          dataUpdated: (cb) => {
            let inter = setInterval(() => {
              if (!this.inProgress) {
                clearInterval(inter)
                cb(this.data)
              }
            }, 10)
          }
        }
        requestCached.push(requestCache)
      } else if (requestCache.inProgress) { // If is in progress
        return requestCache.dataUpdated((dt) => {
          resolve(requestCache.data)
        })
      }
      if (dataResolved) return resolve(dataResolved)
      requestCache.data = null
      requestCache.inProgress = true
      return ajaxGenerator('get', endpoint, undefined, headers).then(dt => {
        requestCache.data = dt
        requestCache.inProgress = false
        return resolve(dt)
      }).catch(err => {
        requestCache.inProgress = false
        return reject(err)
      })
    }),
    put: (endpoint, data, headers) => { return ajaxGenerator('put', endpoint, data, headers) },
    patch: (endpoint, data, headers) => { return ajaxGenerator('patch', endpoint, data, headers) },
    post: (endpoint, data, headers) => { return ajaxGenerator('post', endpoint, data, headers) },
    delete: (endpoint, headers) => { return ajaxGenerator('delete', endpoint, undefined, headers) },
    options: (endpoint, headers) => { return ajaxGenerator('options', endpoint, undefined, headers) },
    head: (endpoint, headers) => { return ajaxGenerator('head', endpoint, undefined, headers) },
    manual: ajaxGenerator
  }

  /**
   * Request generator for ajax XMLHTTPRequest
   * @param {String} method Method of request
   * @param {String} endpoint Enpoint of server
   * @param {Object} data Array or Object to send
   * @param {Object} headers Properties of request in server
   */
  function ajaxGenerator (method, endpoint, data, headers, objectToRequest = {}) {
    objectToRequest = Object.assign({ responseType: 'json' }, objectToRequest)
    if (endpoint && endpoint.charAt(0) === '/' && baseUrl.charAt(baseUrl.length - 1) === '/') endpoint = endpoint.substr(1)
    if (!headers) headers = {}
    var config = {
      method: method,
      data: data,
      // eslint-disable-next-line
      url: baseUrl + (endpoint ? endpoint : '')
    }
    config.headers = headers
    for (var key in generalConfig) {
      if (generalConfig.hasOwnProperty(key)) {
        var hdata = generalConfig[key]
        config[key] = hdata
      }
    }
    return xhr(Object.assign(config, objectToRequest))
  }
}
