const _ = require('lodash')
const unitToSystem = require('./unitToSystem.function');
const binarySystemInvert= {
  '30': 'Gi',
  '20': 'Mi',
  '10': 'Ki',
  '0': '',
  '-10':'mi',
  '-20':'ui'
}

module.exports = unitToBinaryScale

function unitToBinaryScale(config) {
  const number = _.get(config, 'number', 0);
  const unit = _.get(config, 'unit', '');
  return unitToSystem({
    number,
    unit,
    system: binarySystemInvert,
    base: 2,
    expStep: 10
  });
}