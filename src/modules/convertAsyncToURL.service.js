module.exports = convertAsyncToURL;

function convertAsyncToURL (data) {
  return new Promise((resolve, reject) => {
    var reader = new FileReader()
    reader.readAsDataURL(data)
    reader.onloadend = () => resolve(reader.result)
    reader.onerror = (err) => reject(err)
    reader.onabort = () => reject(new Error('Abort URL convertion'))
  })
}
