module.exports = imageCreator

function imageCreator (url) {
  return new Promise((resolve, reject) => {
    var origImg = new Image();
    origImg.src = url;
    origImg.addEventListener("load", () => resolve(origImg));
    origImg.onabort = () => reject('Abort Image')
    origImg.onerror = reject
  })
}