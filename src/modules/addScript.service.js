const ScriptsAdded = []

module.exports = addScript

function addScript (src) {
  return new Promise((resolve, reject) => {
    const added = ScriptsAdded.find(x => x.src === src)
    if (added) return resolve(added.event)
    var s = document.createElement( 'script' )
    s.setAttribute( 'src', src )
    s.onload = (event) => {
      ScriptsAdded.push({src, event})
      setTimeout(() => {
        resolve(event)
      }, 50);
    }
    s.onerror = reject
    s.onabort = reject
    document.body.appendChild( s )
  })
}