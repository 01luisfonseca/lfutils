// Firebase App is always required and must be first
var firebase = require("firebase/app");

// Add additional services that you want to use
require("firebase/auth");
require("firebase/database");
require("firebase/firestore");
require("firebase/messaging");
require("firebase/functions");
require("firebase/storage");

export class Firebase {
  constructor (config) {
    if (!firebase.apps.length) firebase.initializeApp(config);
    else firebase.app();
    this.$fb = firebase
    this.$persistence = false
    var firestore = firebase.firestore()
    // var settings = { timestampsInSnapshots: true }
    // firestore.settings(settings)
    this.$fs = firestore
  }

  async firestoreWithPersistence () {
    if (!window.fsPersistence) {
      await this.$fs.enablePersistence()
      window.fsPersistence = true
      this.$persistence = true
    }
    return this.$fs
  }

  auth () { return this.$fb.auth() }
}
