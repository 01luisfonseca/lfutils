/*!
 * lfutils
 * Copyright(c) 2019 Luis Andrés Fonseca
 * MIT Licensed
 */

module.exports = require('./src/index');
