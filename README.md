# LFUtils

<!-- [![NPM Version][npm-image]][npm-url]
[![NPM Downloads][downloads-image]][downloads-url] -->

This is a group of functionalities to easy handling of **BROWSERS**. for Node JS... test the functionality 😬, without compromises!

## LICENSE

[MIT](LICENSE)

## OPTIONS

### Cacher

The plan is apply cache to every GET request, using XHRRequest. In the request obtain pure response an add it to cache system. Works fine with indexedDB (And is the recomended). We have the functions:

get: (endpoint, headers, cacheFirst, cacheSystem, objectToRequest)
put: (endpoint, data, headers, cacheFirst, cacheSystem, objectToRequest)
patch: (endpoint, data, headers, cacheFirst, cacheSystem, objectToRequest) 
post: (endpoint, data, headers, cacheFirst, cacheSystem, objectToRequest)
delete: (endpoint, headers, cacheFirst, cacheSystem, objectToRequest)
options: (endpoint, headers, cacheFirst, cacheSystem, objectToRequest)
head: (endpoint, headers, cacheFirst, cacheSystem, objectToRequest)

Props:
endpoint => String of URL of the request
headers => Object of request headers

The system mantain the cache if it is required. If a cache isnt required, the system mantain cache op to 24 hours. After, the cache is deleted.
The time to mantain de cache can be setted with variable: window.mediaCache. The format is number. The number indicates the time, in hours, to mantain the cache

The functions works like this:

```javascript
import { cacher } from 'lfutils';

...

cacher.get(/* Params */).then(dt => { /* Actions */ });
```


### Timer

Timer require initialization.

```javascript
import { Timer } from 'lfutils';

...

Timer.initModule();
```

To start timer

```javascript
import { Timer } from 'lfutils';
Timer.initModule();

...

let identificator = Timer.add(() => { ... }, 1000, true); // Time in number, true or false for recursive
```

For finish timer

```javascript
import { Timer } from 'lfutils';
Timer.initModule();

...

Timer.remove(identificator);
```
